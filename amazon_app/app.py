import json
import boto3
from decimal import Decimal
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key
# import requests

class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world",
            # "location": ip.text.replace("\n", "")
        }),
    }

def goodbye_handler(event, context):
    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "You have accessed the Goodbye Function"
        }),
    }

def get_all_records(event, context):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('model_collection')
    table_scan = table.scan()
    records = table_scan['Items']
    print(records)
    response = response = {"statusCode":200,"body":json.dumps(records,cls=DecimalEncoder)}
    return response

# Function that retrieves a record by name
def get_record_by_name(event, context):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('model_collection')
    model_name = event.get("model_name")
    scan_args = {
        'FilterExpression': Key("model_name").eq(model_name)
    }
    try: 
        response = table.scan(**scan_args)
    except ClientError as client_error_exception:
        print(client_error_exception.response['Error']['Message'])
    else:
        return {"statusCode": 200, "body": json.dumps(response['Items'],cls=DecimalEncoder)}    

def create_record(event, context):
    pass
